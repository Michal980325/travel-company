package com.rekrutacja.ZadanieRekrutacja.Model;

public class CitiesDTO {
    private String city;
    private Wether weather;

    public CitiesDTO() {
    }

    public CitiesDTO(String city, Wether weather) {
        this.city = city;
        this.weather = weather;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setWeather(Wether weather) {
        this.weather = weather;
    }

    public String getCity() {
        return city;
    }

    public Wether getWeather() {
        return weather;
    }
}
