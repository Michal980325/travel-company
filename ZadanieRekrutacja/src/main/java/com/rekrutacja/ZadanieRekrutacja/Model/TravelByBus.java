package com.rekrutacja.ZadanieRekrutacja.Model;

public class TravelByBus {

    private  String departureTime;
    private String destinationTime;

    public TravelByBus() {
    }

    public TravelByBus(String departureTime, String destinationTime) {
        this.departureTime = departureTime;
        this.destinationTime = destinationTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public void setDestinationTime(String destinationTime) {
        this.destinationTime = destinationTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public String getDestinationTime() {
        return destinationTime;
    }
}
