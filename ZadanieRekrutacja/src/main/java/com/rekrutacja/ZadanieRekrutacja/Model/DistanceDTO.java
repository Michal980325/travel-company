package com.rekrutacja.ZadanieRekrutacja.Model;

public class DistanceDTO {
    private String source;
    private String destination;
    private int distance;

    public DistanceDTO() {
    }

    public DistanceDTO(String source, String destination, int distance) {
        this.source = source;
        this.destination = destination;
        this.distance = distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public int getDistance() {
        return distance;
    }
}
