package com.rekrutacja.ZadanieRekrutacja.Model;

import java.util.List;

public class BusTravelModelDTO {
    private String source;
    private String destination;
    private List<TravelByBus> travels;

    public BusTravelModelDTO() {
    }

    public BusTravelModelDTO(String source, String destination, List<TravelByBus> travels) {
        this.source = source;
        this.destination = destination;
        this.travels = travels;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setTravels(List<TravelByBus> travels) {
        this.travels = travels;
    }

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public List<TravelByBus> getTravels() {
        return travels;
    }
}
