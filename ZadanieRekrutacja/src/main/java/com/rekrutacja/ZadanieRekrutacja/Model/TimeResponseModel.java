package com.rekrutacja.ZadanieRekrutacja.Model;

public class TimeResponseModel {

    private long time;

    public TimeResponseModel() {
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
