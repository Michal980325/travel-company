package com.rekrutacja.ZadanieRekrutacja.Model;

import java.util.List;

public class OpenBikeDTO {
    private String day;
    private List<CitiesDTO> cities ;

    public OpenBikeDTO() {
    }

    public OpenBikeDTO(String day, List<CitiesDTO> cities) {
        this.day = day;
        this.cities = cities;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setCities(List<CitiesDTO> cities) {
        this.cities = cities;
    }

    public String getDay() {
        return day;
    }

    public List<CitiesDTO> getCities() {
        return cities;
    }
}
