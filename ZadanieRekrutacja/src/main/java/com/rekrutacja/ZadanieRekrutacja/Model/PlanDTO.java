package com.rekrutacja.ZadanieRekrutacja.Model;

import com.rekrutacja.ZadanieRekrutacja.Entity.Segments;

import java.util.List;

public class PlanDTO {
    private int time;
    private List<Segments> segments;

    public PlanDTO(int time, List<Segments> segments) {
        this.time = time;
        this.segments = segments;
    }

    public int getTime() {
        return time;
    }

    public List<Segments> getSegments() {
        return segments;
    }
}
