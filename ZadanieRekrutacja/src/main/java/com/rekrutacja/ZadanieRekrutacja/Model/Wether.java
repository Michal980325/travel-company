package com.rekrutacja.ZadanieRekrutacja.Model;

public class Wether {
    private int wind;
    private int temperature;
    private int probabilityOfPrecipitation;

    public Wether() {
    }

    public Wether(int wind, int temperature, int probabilityOfPrecipitation) {
        this.wind = wind;
        this.temperature = temperature;
        this.probabilityOfPrecipitation = probabilityOfPrecipitation;
    }

    public void setWind(int wind) {
        this.wind = wind;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public void setProbabilityOfPrecipitation(int probabilityOfPrecipitation) {
        this.probabilityOfPrecipitation = probabilityOfPrecipitation;
    }

    public int getWind() {
        return wind;
    }

    public int getTemperature() {
        return temperature;
    }

    public int getProbabilityOfPrecipitation() {
        return probabilityOfPrecipitation;
    }
}
