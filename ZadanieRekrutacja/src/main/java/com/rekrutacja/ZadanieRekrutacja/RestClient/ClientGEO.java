package com.rekrutacja.ZadanieRekrutacja.RestClient;

import com.rekrutacja.ZadanieRekrutacja.Model.DistanceDTO;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class ClientGEO {
    private RestTemplate restTemplateGEO = new RestTemplate();


    public List<DistanceDTO> getAllDistances(){
        DistanceList response =  restTemplateGEO.getForObject("http://resources.codeconcept.pl/api/distance/", DistanceList.class);
        List<DistanceDTO> distanceDTOList = response.getDistances();
        return  distanceDTOList;
    }
}
