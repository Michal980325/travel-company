package com.rekrutacja.ZadanieRekrutacja.RestClient;

import com.rekrutacja.ZadanieRekrutacja.Model.BusTravelModelDTO;

import java.util.ArrayList;
import java.util.List;

public class BusTravelsList {
    List<BusTravelModelDTO> timetable;

    public BusTravelsList() {
    timetable = new ArrayList<>();
    }
    public BusTravelsList(List<BusTravelModelDTO> timetable) {
        this.timetable = timetable;
    }

    public List<BusTravelModelDTO> getTimetable() {
        return timetable;
    }
}
