package com.rekrutacja.ZadanieRekrutacja.RestClient;

import com.rekrutacja.ZadanieRekrutacja.Model.BusTravelModelDTO;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class ClientSpeed {
    RestTemplate restTemplateSpeed = new RestTemplate();

   public List<BusTravelModelDTO> getAllTravelsByBus(){
        var response = restTemplateSpeed.getForObject("http://resources.codeconcept.pl/api/timetable/",BusTravelsList.class);
        List<BusTravelModelDTO> busTravelsList = response.getTimetable();
        return busTravelsList;

   }
}
