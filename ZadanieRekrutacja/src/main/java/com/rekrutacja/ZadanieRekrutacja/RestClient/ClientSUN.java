package com.rekrutacja.ZadanieRekrutacja.RestClient;

import com.rekrutacja.ZadanieRekrutacja.Model.OpenBikeDTO;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class ClientSUN {
    private RestTemplate restTemplateSUN = new RestTemplate();

   public List<OpenBikeDTO> getAllinfo(){
        var responseFromSUN= restTemplateSUN.getForObject("http://resources.codeconcept.pl/api/weather/", BikeDTOList.class);
        List<OpenBikeDTO> list = responseFromSUN.getDays();
        return list;
    }
}
