package com.rekrutacja.ZadanieRekrutacja.RestClient;

import com.rekrutacja.ZadanieRekrutacja.Model.DistanceDTO;

import java.util.ArrayList;
import java.util.List;

public class DistanceList {

    private List<DistanceDTO> distances;

    public DistanceList() {
        this.distances = new ArrayList<>();
    }

    public DistanceList(List<DistanceDTO> distances) {
        this.distances = distances;
    }

    public List<DistanceDTO> getDistances() {
        return distances;
    }

}
