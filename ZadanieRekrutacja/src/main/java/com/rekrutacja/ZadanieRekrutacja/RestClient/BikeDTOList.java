package com.rekrutacja.ZadanieRekrutacja.RestClient;

import com.rekrutacja.ZadanieRekrutacja.Model.OpenBikeDTO;

import java.util.ArrayList;
import java.util.List;

public class BikeDTOList {

   private List<OpenBikeDTO> days;

    public BikeDTOList() {
        days= new ArrayList<>();
    }

    public BikeDTOList(List<OpenBikeDTO> openBikeDTOList) {
        this.days = openBikeDTOList;
    }

    public List<OpenBikeDTO> getDays() {
        return days;
    }
}
