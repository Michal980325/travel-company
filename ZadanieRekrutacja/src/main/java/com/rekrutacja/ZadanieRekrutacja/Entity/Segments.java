package com.rekrutacja.ZadanieRekrutacja.Entity;

import javax.persistence.*;

@Entity
public class Segments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String type;
    private String city;

    @ManyToOne(cascade = CascadeType.ALL)
    private PlanEntity plan;

    public Segments() {
    }

    public Segments(String type, String city) {
        this.type = type;
        this.city = city;

    }

    public PlanEntity getPlan() {
        return plan;
    }

    public void setPlan(PlanEntity plan) {
        this.plan = plan;
    }

    public String getType() {
        return type;
    }

    public String getCity() {
        return city;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
