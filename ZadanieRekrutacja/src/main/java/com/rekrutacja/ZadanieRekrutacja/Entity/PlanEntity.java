package com.rekrutacja.ZadanieRekrutacja.Entity;


import javax.persistence.*;
import java.util.List;

@Entity
public class PlanEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int time;

    @OneToMany(mappedBy = "plan")
    private List<Segments> segments;

    public PlanEntity() {
    }

    public PlanEntity(int time, List<Segments> segments) {
        this.time = time;
        this.segments = segments;
    }

    public void setSegments(List<Segments> segments) {
        this.segments = segments;
    }

    public int getId() {
        return id;
    }

    public int getTime() {
        return time;
    }

    public List<Segments> getSegments() {
        return segments;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
