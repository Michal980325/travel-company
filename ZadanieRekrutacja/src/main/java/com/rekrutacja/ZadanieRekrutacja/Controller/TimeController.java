package com.rekrutacja.ZadanieRekrutacja.Controller;

import com.rekrutacja.ZadanieRekrutacja.Service.TimeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/time")
public class TimeController {
    private TimeService timeService;

    public TimeController(TimeService timeService) {
        this.timeService = timeService;
    }

    @GetMapping("/walk/{source}/{destination}")
    ResponseEntity<?> getWalkTime(@PathVariable("source") String source,
                                  @PathVariable("destination") String destination){

       return ResponseEntity.ok(timeService.getWalkingTime(source,destination));
    }

    @GetMapping("/bus/{source}/{destination}")
    ResponseEntity<?> getTravelByBus(@PathVariable("source") String source,
                                     @PathVariable("destination") String destination) {


        return ResponseEntity.ok(timeService.getDriveByBusTime(source,destination));
    }

    @GetMapping("/bike/{source}/{destination}/{day}")
    ResponseEntity<?> getTravelByBike(@PathVariable("source") String source,
                                                      @PathVariable("destination") String destination,
                                                      @PathVariable("day") String day){

        return ResponseEntity.ok(timeService.getDriveByBike(source,destination,day));
    }

}
