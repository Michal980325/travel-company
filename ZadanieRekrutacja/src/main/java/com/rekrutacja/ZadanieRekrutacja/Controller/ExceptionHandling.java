package com.rekrutacja.ZadanieRekrutacja.Controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.DateTimeException;
import java.util.NoSuchElementException;

@ControllerAdvice
public class ExceptionHandling  {

    @ExceptionHandler({NoSuchElementException.class})
    ResponseEntity<String> handllerNoElement(NoSuchElementException e) {
        return ResponseEntity.badRequest().build();
    }
    @ExceptionHandler({DateTimeException.class})
    ResponseEntity<String> handllerBadDate(DateTimeException e) {
        return ResponseEntity.badRequest().build();
    }
    @ExceptionHandler({IllegalArgumentException.class})
    ResponseEntity<String> handllerBadDate(IllegalArgumentException e) {
        return ResponseEntity.notFound().build();
    }
}
