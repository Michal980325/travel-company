package com.rekrutacja.ZadanieRekrutacja.Controller;

import com.rekrutacja.ZadanieRekrutacja.Model.TimeResponseModel;
import com.rekrutacja.ZadanieRekrutacja.Service.TravelService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/travel")
public class TravelController {
    private TravelService travelService;

    public TravelController(TravelService travelService) {
        this.travelService = travelService;
    }

    @GetMapping("/bus/{source}/{destination}/{day}/{time}")
    ResponseEntity<?> getShortestRoadByBus(@PathVariable("source") String source,
                                                           @PathVariable("destination") String destination,
                                                           @PathVariable("day") String day,
                                                           @PathVariable("time") String time) {
        Optional<TimeResponseModel> response = travelService.getShortestTraseByBus(source,destination,day,time);


        return   ResponseEntity.ok(response);

    }

    @GetMapping("/bike/{source}/{destination}/{day}")
    ResponseEntity<?> getShortestRoadByBike(@PathVariable("source") String source,
                                           @PathVariable("destination") String destination,
                                           @PathVariable("day") String day){
        Optional<TimeResponseModel> response = travelService.getShortestTraseByBike(source,destination,day);


        return   ResponseEntity.ok(response);
    }



}
