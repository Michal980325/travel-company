package com.rekrutacja.ZadanieRekrutacja.Controller;

import com.rekrutacja.ZadanieRekrutacja.Model.PlanDTO;
import com.rekrutacja.ZadanieRekrutacja.Repository.RepoForSegments;
import com.rekrutacja.ZadanieRekrutacja.Service.PlanService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/plan")
public class PlanController {
    private PlanService service ;


    public PlanController(PlanService service, RepoForSegments repo) {
        this.service = service;

    }

    @PostMapping("/{source}/{destination}/{day}/{time}")
    ResponseEntity<?> addNewPlan(
            @PathVariable("source") String source,
    @PathVariable("destination") String destination,
    @PathVariable("day") String day,
    @PathVariable("time") String time
    ) {
        service.save(source,destination,day,time);
        return ResponseEntity.ok().build();
    }
    @GetMapping("/{id}")
    ResponseEntity<PlanDTO> findPlan(@PathVariable int id){
        return ResponseEntity.ok(service.findPlanById(id));
    }


}
