package com.rekrutacja.ZadanieRekrutacja.Aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class LoggerAspect {

    private static final Logger logger= LoggerFactory.getLogger(LoggerAspect.class);

    @Pointcut("execution(* com.rekrutacja.ZadanieRekrutacja.Controller.PlanController..*(..))")
    static void planController(){}
    @Pointcut("execution(* com.rekrutacja.ZadanieRekrutacja.Controller.TravelController..*(..))")
    static void travelController(){}
    @Pointcut("execution(* com.rekrutacja.ZadanieRekrutacja.Controller.TimeController..*(..))")
     static void timeContoller(){}


     @Before("timeContoller() || planController() || travelController()")
    void infoMethodWalkTimeB(JoinPoint jp){
         HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                 .currentRequestAttributes())
                 .getRequest();
         logger.info("Http method: {}", request.getMethod());
    }

     @After("timeContoller() || planController() || travelController()")
     void infoMethodWalkTime(JoinPoint jP){

         logger.info("Method Path {} ", jP.getSignature().getDeclaringType() );

         int response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                 .getResponse().getStatus();
         logger.info("HTTP STATUS {} ", response);
     }

}
