package com.rekrutacja.ZadanieRekrutacja.Service;

import com.rekrutacja.ZadanieRekrutacja.Entity.PlanEntity;
import com.rekrutacja.ZadanieRekrutacja.Model.PlanDTO;
import com.rekrutacja.ZadanieRekrutacja.Repository.RepoForProject;
import com.rekrutacja.ZadanieRekrutacja.Repository.RepoForSegments;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlanService  {


    private RepoForSegments repo;
    private RepoForProject repoForProject;

    private  TravelService travelService;
    public PlanService(RepoForSegments repo, RepoForProject repoForProject, TravelService travelService) {
        this.repo = repo;
        this.repoForProject = repoForProject;
        this.travelService = travelService;
    }

    public void save(String source, String destination, String day, String time){
        List<String> cities = travelService.findAllCity();
        travelService.makeMapWithEvryPosibleRoadBetweenSourceAndDestination(cities,source,destination,false,time,
                true,day,repo);
    }


    public PlanDTO findPlanById(int id)  {

        if(!repoForProject.existsById(id)){
         throw new IllegalArgumentException();
        }
        PlanEntity planEntity = repoForProject.findById(id).get();
        PlanDTO planDTO = new PlanDTO(planEntity.getTime(),planEntity.getSegments());
        return planDTO;
    }
}
