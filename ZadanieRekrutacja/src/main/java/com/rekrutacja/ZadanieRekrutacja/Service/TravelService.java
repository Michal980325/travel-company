package com.rekrutacja.ZadanieRekrutacja.Service;

import com.rekrutacja.ZadanieRekrutacja.Entity.PlanEntity;
import com.rekrutacja.ZadanieRekrutacja.Entity.Segments;
import com.rekrutacja.ZadanieRekrutacja.Model.*;
import com.rekrutacja.ZadanieRekrutacja.Repository.RepoForSegments;
import com.rekrutacja.ZadanieRekrutacja.RestClient.ClientGEO;
import com.rekrutacja.ZadanieRekrutacja.RestClient.ClientSUN;
import com.rekrutacja.ZadanieRekrutacja.RestClient.ClientSpeed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


@Service
public class TravelService extends  SuperService{
    private ClientGEO clientGEO;
    private ClientSpeed clientSpeed;
    private ClientSUN clientSUN;
    private static final Logger logger = LoggerFactory.getLogger(TravelService.class);


    public TravelService(ClientGEO clientGEO, ClientSpeed clientSpeed, ClientSUN clientSUN) {
        this.clientGEO = clientGEO;
        this.clientSpeed = clientSpeed;
        this.clientSUN = clientSUN;
    }

    public Optional<TimeResponseModel> getShortestTraseByBus(String source, String destination, String day, String time){

        long timeToBeat = driveTimeFromCityToCityByBus(source,destination,time);


        List<String> cites = findAllCity();
        cites.remove(source);
        cites.remove(destination);
        Map<List<String>, Integer> mapWithDataOfDriveTime =
                makeMapWithEvryPosibleRoadBetweenSourceAndDestination(cites,source,destination,true,time,false,day,null);

        for(Map.Entry<List<String>, Integer> map : mapWithDataOfDriveTime.entrySet()){
            if(timeToBeat> map.getValue())
            {
                timeToBeat=map.getValue();
            }
        }
        TimeResponseModel response = new TimeResponseModel();
        response.setTime(timeToBeat);
        return Optional.of(response);
    }


    public Optional<TimeResponseModel> getShortestTraseByBike(String source, String destination, String day){
        List<String> cities = findAllCity();
        List<OpenBikeDTO> responseFromSUN = clientSUN.getAllinfo();
        List<String>  availableCitiesInTheesDay = new ArrayList<>();
        int distanceToBeat;
        int timeToBeat;
        List<DistanceDTO> responseFromGEO = clientGEO.getAllDistances();

        Optional<DistanceDTO> roadBetweenSoruceAndDestination = roadBetweenCitys(source,destination,responseFromGEO);

        if(roadBetweenSoruceAndDestination.isEmpty()){
            return Optional.empty();
        }
        distanceToBeat=roadBetweenSoruceAndDestination.get().getDistance();
        timeToBeat= (int) timeInMinutsForDistance(distanceToBeat,20);
        for(String city : cities){
            if(!checkWether(city,day,responseFromSUN)){
                availableCitiesInTheesDay.add(city);
            }
        }
        if(availableCitiesInTheesDay.isEmpty() || !availableCitiesInTheesDay.contains(source) || !availableCitiesInTheesDay.contains(destination)){
            throw  new NoSuchElementException();
        }

        availableCitiesInTheesDay.remove(source);
        availableCitiesInTheesDay.remove(destination);
       Map<List<String> , Integer>  mapOfTimeForEvryRoad =
               makeMapWithEvryPosibleRoadBetweenSourceAndDestination(availableCitiesInTheesDay,source,destination,false,null,false,null,null);

       for(Map.Entry<List<String> , Integer> map : mapOfTimeForEvryRoad.entrySet()){
           if(timeToBeat > map.getValue()){
               timeToBeat=map.getValue();
           }
       }
       TimeResponseModel response = new TimeResponseModel();
       response.setTime(timeToBeat);

        return Optional.of(response);
    }

     List<String> findAllCity() {
        List<OpenBikeDTO> responseFromSun = clientSUN.getAllinfo();
        List<String> allCity = new ArrayList<>();
        responseFromSun.get(0).getCities().forEach(y -> allCity.add(y.getCity()));
        return allCity;
    }

     long driveTimeFromCityToCityByBus(String startCity, String finishCity, String startTime ){
        List<BusTravelModelDTO> responseFromSpeed=clientSpeed.getAllTravelsByBus();
        long timeTravelToDestination = -1 ;
        List<Long> timeList = new ArrayList<>();
        List<TravelByBus>  busFromSourceToDestination = new ArrayList<>();

        for (BusTravelModelDTO x :responseFromSpeed){
            if (x.getSource().equals(startCity) && x.getDestination().equals(finishCity)) {
                busFromSourceToDestination=x.getTravels();
                break;}
        }

        if(busFromSourceToDestination.isEmpty()){
            return timeTravelToDestination;
        }


        busFromSourceToDestination.stream().forEach(c -> {

            long waitingTime =  durationTimeInMinuts(startTime,c.getDepartureTime());
            long draivingTime =  durationTimeInMinuts(c.getDepartureTime(),c.getDestinationTime());
            long allTime =waitingTime+draivingTime;
            if(waitingTime >=0 && draivingTime >= 0){
            timeList.add(allTime);}
        });

        timeTravelToDestination = timeList.stream().min(Long::compareTo).orElse(-1l);

        return timeTravelToDestination;
    }


     String addToHours(String h1,Long plusMinuts){
        LocalTime hour = LocalTime.parse(h1);
        hour=hour.plusMinutes(plusMinuts);
        return hour.format(DateTimeFormatter.ofPattern("HH:mm"));
    }


    Map<List<String>, Integer> makeMapWithEvryPosibleRoadBetweenSourceAndDestination
             (List<String> cities, String source, String destination, boolean ifByBus, String busStart,
              boolean ifDoPaln, String day, RepoForSegments repo){

        List<DistanceDTO> responseFromGEO = clientGEO.getAllDistances();
        List<OpenBikeDTO> responseFromSun = clientSUN.getAllinfo();
        Map<List<String>, Integer> evryPosibleRoad  = new LinkedHashMap<>();
        List<String> tempCity = new ArrayList<>();
        List<String> citiesAvailableForBike= new LinkedList<>();
        Random random = new Random();

        for(String x : cities){
            if(!checkWether(x,day,responseFromSun)){
                citiesAvailableForBike.add(x);
            }
        }

        int preSize=0;

                for(int n=0;n<cities.size();n++){

                    preSize= preSize+ factorial(cities.size(),cities.size()-n);
                    int timeForRoad=0 ;
                    while (true) {
                        List<String> citiesList = new ArrayList<>();
                        PlanEntity plan = new PlanEntity();
                        List<Segments> segmentsList = new ArrayList<>();
                        citiesList.add(source);
                        tempCity.clear();
                        tempCity.addAll(cities);
                        List<Integer> preRandom = new ArrayList<>();
                        preRandom.clear();

                        for (int m = 0; m <= n; m++) {

                                int temp = random.nextInt(tempCity.size());
                                citiesList.add(tempCity.get(temp));

                                if(ifDoPaln) {
                                    Segments segments = new Segments();
                                    Optional<DistanceDTO> distanceDTO = roadBetweenCitys(citiesList.get(citiesList.size()-2),citiesList.get(citiesList.size()-1),responseFromGEO);
                                    int disnatnce =0  ;
                                    int timeForBus= (int) driveTimeFromCityToCityByBus(citiesList.get(citiesList.size()-2),citiesList.get(citiesList.size()-1),addToHours(busStart, (long) timeForRoad));
                                   int timeForWalk = 20000;
                                    if(distanceDTO.isPresent() ) {
                                         timeForWalk = (int) timeInMinutsForDistance(disnatnce, 6);
                                    }
                                    int timeForBike = -1;

                                    if(citiesAvailableForBike.containsAll(citiesList) && disnatnce > 0){
                                        timeForBike= (int) timeInMinutsForDistance(disnatnce,20); }
                                    if(timeForBus <= timeForWalk){
                                        if(timeForBike<0 || (timeForBike >0 && timeForBike>= timeForBus) && disnatnce > 0){

                                            timeForRoad += timeForBus;
                                            segments.setType("bus");
                                            segments.setCity(citiesList.get(citiesList.size()-1));
                                        }
                                        if(timeForBike>0  && timeForBike < timeForBus && disnatnce > 0){
                                            timeForRoad += timeForBike;
                                            segments.setType("bike");
                                            segments.setCity(citiesList.get(citiesList.size()-1));
                                        }

                                    }else {
                                        if(timeForBike<0 || (timeForBike >0 && timeForBike >= timeForWalk && disnatnce > 0)){

                                            timeForRoad += timeForWalk;
                                            segments.setType("walk");
                                            segments.setCity(citiesList.get(citiesList.size()-1));
                                        }
                                        if(timeForBike>0  && timeForBike < timeForWalk && disnatnce > 0){
                                            timeForRoad += timeForBike;
                                            segments.setType("bike");
                                            segments.setCity(citiesList.get(citiesList.size()-1));
                                        }
                                    }
                                    if (disnatnce == 0){
                                        timeForRoad += timeForBus;
                                        segments.setType("bus");
                                        segments.setCity(citiesList.get(citiesList.size()-1));
                                    }

                                    segmentsList.add(segments);

                                }else {

                                if(ifByBus){
                                     timeForRoad += (int) driveTimeFromCityToCityByBus(citiesList.get(citiesList.size()-2),citiesList.get(citiesList.size()-1),addToHours(busStart, (long) timeForRoad));


                                }else {
                                    int disnatnce = roadBetweenCitys(citiesList.get(citiesList.size()-2),citiesList.get(citiesList.size()-1),responseFromGEO).get().getDistance();
                                    timeForRoad += (int) timeInMinutsForDistance(disnatnce,20);
                                }
                                }

                                tempCity.remove(temp);
                        }
                        if (!evryPosibleRoad.containsKey(citiesList)) {
                            citiesList.add(destination);
                            if (ifDoPaln) {
                                Segments segments = new Segments();
                                Optional<DistanceDTO> dtoOptional = roadBetweenCitys(citiesList.get(citiesList.size() - 2), citiesList.get(citiesList.size() - 1), responseFromGEO);
                                int disnatnce;
                                int timeForWalk=0;
                                if(dtoOptional.isPresent()){
                                 disnatnce= dtoOptional.get().getDistance();}else {
                                    disnatnce=0;
                                }

                                int timeForBus = (int) driveTimeFromCityToCityByBus(citiesList.get(citiesList.size() - 2), citiesList.get(citiesList.size() - 1), addToHours(busStart, (long) timeForRoad));
                                if(disnatnce >=0) {
                                 timeForWalk=(int) timeInMinutsForDistance(disnatnce, 6);
                                }
                              int timeForBike = -1;

                                if (citiesAvailableForBike.containsAll(citiesList) && disnatnce >0) {
                                    timeForBike = (int) timeInMinutsForDistance(disnatnce, 20);
                                }

                                if (timeForBus <= timeForWalk && disnatnce >0) {
                                    if (timeForBike < 0 || (timeForBike > 0 && timeForBike >= timeForBus)) {

                                        timeForRoad += timeForBus;
                                        segments.setType("bus");
                                        segments.setCity(citiesList.get(citiesList.size() - 1));
                                    }
                                    if (timeForBike > 0 && timeForBike < timeForBus && disnatnce >0) {
                                        timeForRoad += timeForBike;
                                        segments.setType("bike");
                                        segments.setCity(citiesList.get(citiesList.size() - 1));
                                    }

                                } else {
                                    if (timeForBike < 0 || (timeForBike > 0 && timeForBike >= timeForWalk) && disnatnce >0) {

                                        timeForRoad += timeForWalk;
                                        segments.setType("walk");
                                        segments.setCity(citiesList.get(citiesList.size() - 1));
                                    }
                                    if (timeForBike > 0 && timeForBike < timeForWalk && disnatnce >0) {
                                        timeForRoad += timeForBike;
                                        segments.setType("bike");
                                        segments.setCity(citiesList.get(citiesList.size() - 1));
                                    }
                                }

                                if(disnatnce ==0){
                                    timeForRoad += timeForBus;
                                    segments.setType("bus");
                                    segments.setCity(citiesList.get(citiesList.size() - 1));
                                }

                                segmentsList.add(segments);
                                plan.setTime(timeForRoad);

                                segmentsList.forEach(x -> x.setPlan(plan));
                                segmentsList.forEach(x -> repo.save(x));

                            } else {

                                if (ifByBus) {

                                    timeForRoad += (int) driveTimeFromCityToCityByBus(citiesList.get(citiesList.size() - 2), citiesList.get(citiesList.size() - 1), addToHours(busStart, (long) timeForRoad));

                                } else {

                                    int disnatnce = roadBetweenCitys(citiesList.get(citiesList.size() - 2), citiesList.get(citiesList.size() - 1), responseFromGEO).get().getDistance();
                                    timeForRoad += (int) timeInMinutsForDistance(disnatnce, 20);
                                }
                                evryPosibleRoad.put(citiesList, timeForRoad);
                            }
                        }

                        if(evryPosibleRoad.size() == preSize){
                            break;
                        }

                    }
                }
                return evryPosibleRoad;
    }

     int factorial(int n, int m){
        int wynik = 1;

        for(int i=n;i>=m;i--){
            wynik*=i;
        }
return wynik;
}

}
