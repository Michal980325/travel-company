package com.rekrutacja.ZadanieRekrutacja.Service;

import com.rekrutacja.ZadanieRekrutacja.Model.CitiesDTO;
import com.rekrutacja.ZadanieRekrutacja.Model.DistanceDTO;
import com.rekrutacja.ZadanieRekrutacja.Model.OpenBikeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Component
public class SuperService {
    private static final Logger logger = LoggerFactory.getLogger(SuperService.class);
     double timeInMinutsForDistance(int disnace, double avgVelocity){
        return disnace/(avgVelocity/60);
    }

    long durationTimeInMinuts(String start, String finish)  {
        LocalTime startDrive = LocalTime.parse(start);
        LocalTime finishDrive = LocalTime.parse(finish);
        Duration duration = Duration.between(startDrive,finishDrive);
        long timeInMinuts = duration.toMinutes();
        return  timeInMinuts;
    }

     boolean checkWether(String city, String day, List<OpenBikeDTO> responseFromSUN){

        Optional<CitiesDTO> cityToCheck = Optional.empty() ;

        for(OpenBikeDTO x :responseFromSUN){
            if(x.getDay().equals(day)){

                Optional<CitiesDTO> ifCityExist = x.getCities().stream()
                        .filter(y -> y.getCity().equals(city)).findAny();

                if(ifCityExist.isPresent()){
                    cityToCheck = Optional.ofNullable(ifCityExist.get());
                    break;}
            }
        }
        if(cityToCheck.isEmpty())
        {
            return true;
        }
        Optional<CitiesDTO> checkCity = cityToCheck.stream()
                .filter(c -> (c.getWeather().getTemperature()>=10 && c.getWeather().getTemperature() <=25
                        && c.getWeather().getProbabilityOfPrecipitation() < 50 && c.getWeather().getWind()<70))
                .findAny();
        if(checkCity.isEmpty())
        {
            return true;
        }
        return false;
    }

     Optional<DistanceDTO> roadBetweenCitys(String source, String destination, List<DistanceDTO> responseFromGEO){

        Optional<DistanceDTO> roadBetweenSoruceAndDestination = responseFromGEO.stream()
                .filter(e -> ((e.getSource().equals(source) && e.getDestination().equals(destination))
                || (e.getSource().equals(destination) && e.getDestination().equals(source))))
                .findAny();
        if(roadBetweenSoruceAndDestination.isEmpty()) {
            return Optional.empty();
        }
        return  roadBetweenSoruceAndDestination;
    }



}
