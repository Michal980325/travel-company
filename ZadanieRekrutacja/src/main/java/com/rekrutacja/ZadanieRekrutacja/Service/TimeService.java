package com.rekrutacja.ZadanieRekrutacja.Service;

import com.rekrutacja.ZadanieRekrutacja.Model.BusTravelModelDTO;
import com.rekrutacja.ZadanieRekrutacja.Model.DistanceDTO;
import com.rekrutacja.ZadanieRekrutacja.Model.OpenBikeDTO;
import com.rekrutacja.ZadanieRekrutacja.Model.TimeResponseModel;
import com.rekrutacja.ZadanieRekrutacja.RestClient.ClientGEO;
import com.rekrutacja.ZadanieRekrutacja.RestClient.ClientSUN;
import com.rekrutacja.ZadanieRekrutacja.RestClient.ClientSpeed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class TimeService extends SuperService{
    private static final Logger logger = LoggerFactory.getLogger(TimeService.class);
    private ClientGEO clientGEO;
    private ClientSpeed clientSpeed;
    private ClientSUN clientSUN;

    public TimeService(ClientGEO clientGEO, ClientSpeed clientSpeed, ClientSUN clientSUN) {
        this.clientGEO = clientGEO;
        this.clientSpeed = clientSpeed;
        this.clientSUN = clientSUN;
    }



    public Optional<TimeResponseModel> getWalkingTime(String source, String destination){
        List<DistanceDTO> responseFromGEO = clientGEO.getAllDistances();
        Optional<DistanceDTO> roadBetweenSoruceAndDestination = roadBetweenCitys(source,destination,responseFromGEO);

        TimeResponseModel response =  new TimeResponseModel();
        int walkTime= (int) timeInMinutsForDistance(roadBetweenSoruceAndDestination.get().getDistance(),6.0);
        response.setTime(walkTime);
        return Optional.of(response);
    }

    public Optional<TimeResponseModel> getDriveByBusTime(String source, String destination) {
        List<BusTravelModelDTO> responseFromSpeed=clientSpeed.getAllTravelsByBus();
        Optional<BusTravelModelDTO> roadBetweenSoruceAndDestination =
                responseFromSpeed.stream()
                        .filter(e -> (e.getSource().equals(source) && e.getDestination().equals(destination)))
                        .findAny();



        String startDrive =roadBetweenSoruceAndDestination.get().getTravels()
                .get(0).getDepartureTime();
        String finishDrive = roadBetweenSoruceAndDestination.get().getTravels()
                .get(0).getDestinationTime();

        TimeResponseModel response =  new TimeResponseModel();
        int driveTime= (int) durationTimeInMinuts(startDrive,finishDrive);
        response.setTime(driveTime);
        return Optional.of(response);
    }

    public  Optional<TimeResponseModel> getDriveByBike(String source, String destination, String day){
        List<DistanceDTO> responseFromGEO = clientGEO.getAllDistances();
        Optional<DistanceDTO> roadBetweenSoruceAndDestination = roadBetweenCitys(source,destination,responseFromGEO);
        List<OpenBikeDTO> responseFromSUN = clientSUN.getAllinfo();

        if(roadBetweenSoruceAndDestination.isEmpty() || checkWether(source,day,responseFromSUN) || checkWether(destination,day,responseFromSUN)){
           throw new NoSuchElementException();
        }

        TimeResponseModel response =  new TimeResponseModel();
        int bikeTime= (int) timeInMinutsForDistance(roadBetweenSoruceAndDestination.get().getDistance(),25.0);
        response.setTime(bikeTime);
        return Optional.of(response);
    }





}
