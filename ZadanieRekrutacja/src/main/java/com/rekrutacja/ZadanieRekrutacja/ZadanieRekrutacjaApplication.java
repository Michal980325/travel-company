package com.rekrutacja.ZadanieRekrutacja;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZadanieRekrutacjaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZadanieRekrutacjaApplication.class, args);
	}

}
