package com.rekrutacja.ZadanieRekrutacja.Repository;

import com.rekrutacja.ZadanieRekrutacja.Entity.Segments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepoForSegments extends JpaRepository<Segments,Integer> {
}
