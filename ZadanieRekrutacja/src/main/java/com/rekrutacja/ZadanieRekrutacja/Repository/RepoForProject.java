package com.rekrutacja.ZadanieRekrutacja.Repository;

import com.rekrutacja.ZadanieRekrutacja.Entity.PlanEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepoForProject extends JpaRepository<PlanEntity,Integer> {
}
