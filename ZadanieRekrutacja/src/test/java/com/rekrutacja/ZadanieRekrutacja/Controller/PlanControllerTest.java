package com.rekrutacja.ZadanieRekrutacja.Service;

import com.rekrutacja.ZadanieRekrutacja.Entity.PlanEntity;
import com.rekrutacja.ZadanieRekrutacja.Entity.Segments;
import com.rekrutacja.ZadanieRekrutacja.Model.PlanDTO;
import com.rekrutacja.ZadanieRekrutacja.Repository.RepoForProject;
import com.rekrutacja.ZadanieRekrutacja.Repository.RepoForSegments;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2")
class PlanControllerTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    RepoForSegments repo;

    @Autowired
    RepoForProject repoForProject;

    @Test
    void testSaveToRepo(){

        PlanEntity planEntity = new PlanEntity();
        planEntity.setTime(20);
        Segments segment = new Segments();
        segment.setCity("A");
        segment.setType("B");
        segment.setPlan(planEntity);

        repo.deleteAll();
        repoForProject.deleteAll();
        assertEquals(0,repo.count());
        assertEquals(0,repoForProject.count());
        repo.save(segment);
        assertEquals(1,repo.count());
        assertEquals(1,repoForProject.count());
    }
    @Test
    void httpGet_returnsOnePlan(){
        repo.deleteAll();
        repoForProject.deleteAll();
        assertEquals(0,repo.count());
        assertEquals(0,repoForProject.count());


        PlanEntity planEntity = new PlanEntity();
        planEntity.setTime(20);
        Segments segment = new Segments();
        segment.setCity("A");
        segment.setType("B");
        segment.setPlan(planEntity);

        PlanEntity planEntity2 = new PlanEntity();
        planEntity2.setTime(30);
        Segments segment2 = new Segments();
        segment2.setCity("C");
        segment2.setType("D");
        segment2.setPlan(planEntity2);

        repo.save(segment);
        repo.save(segment2);

        assertEquals(2,repo.count());
        assertEquals(2,repoForProject.count());

        PlanDTO result =  restTemplate.getForObject("http://localhost:" + port + "/plan/1", PlanDTO.class);

        assertEquals(20,result.getTime());

    }
}