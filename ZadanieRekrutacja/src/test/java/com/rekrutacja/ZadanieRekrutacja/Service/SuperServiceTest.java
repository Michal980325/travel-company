package com.rekrutacja.ZadanieRekrutacja.Service;

import com.rekrutacja.ZadanieRekrutacja.Model.CitiesDTO;
import com.rekrutacja.ZadanieRekrutacja.Model.DistanceDTO;
import com.rekrutacja.ZadanieRekrutacja.Model.OpenBikeDTO;
import com.rekrutacja.ZadanieRekrutacja.Model.Wether;
import com.rekrutacja.ZadanieRekrutacja.RestClient.ClientGEO;
import com.rekrutacja.ZadanieRekrutacja.RestClient.ClientSUN;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SuperServiceTest {

    SuperService service = new SuperService();
    @Test
    void mesuring_Time_With_Velocity(){
        var toTest =  service.timeInMinutsForDistance(1,1);
        assertEquals(60,toTest);
    }
    @Test
    void mesuring_Duration_Time(){
        String t1 = "12:00";
        String t2 = "12:20";

        var toTest =service.durationTimeInMinuts(t1,t2);

        assertEquals(20,toTest);
    }

    @Test
    void check_Wether_With_NO_City(){
        var mockClient = mock(ClientSUN.class);
        when(mockClient.getAllinfo()).thenReturn(List.of());
        String city = "CC";

        var toTest = service.checkWether(city,null,mockClient.getAllinfo());

        assertThat(Optional.empty());
    }

    @Test
    void check_Wether_With_Bad_Wind(){
        var objectDTO= new OpenBikeDTO();
        var listCity = new CitiesDTO();
        var wetherToTest = new Wether();
        wetherToTest.setWind(100);
        wetherToTest.setProbabilityOfPrecipitation(0);
        wetherToTest.setTemperature(20);
        listCity.setCity("CC");
        listCity.setWeather(wetherToTest);
        objectDTO.setDay("m");
        objectDTO.setCities(List.of(listCity));
        String city = "CC";
        String day = "m";

        var mockClient = mock(ClientSUN.class);
        when(mockClient.getAllinfo()).thenReturn(List.of(objectDTO));

        var toTest = service.checkWether(city,day,mockClient.getAllinfo());

        assertThat(Optional.empty());

    }

    @Test
    void check_Wether_With_Bad_Temperture(){
        var objectDTO= new OpenBikeDTO();
        var listCity = new CitiesDTO();
        var wetherToTest = new Wether();
        wetherToTest.setWind(30);
        wetherToTest.setProbabilityOfPrecipitation(0);
        wetherToTest.setTemperature(30);
        listCity.setCity("CC");
        listCity.setWeather(wetherToTest);
        objectDTO.setDay("m");
        objectDTO.setCities(List.of(listCity));
        String city = "CC";
        String day = "m";

        var mockClient = mock(ClientSUN.class);
        when(mockClient.getAllinfo()).thenReturn(List.of(objectDTO));

        var toTest = service.checkWether(city,day,mockClient.getAllinfo());

        assertThat(Optional.empty());
    }

    @Test
    void check_Wether_With_Bad_Probability_Rain(){
        var objectDTO= new OpenBikeDTO();
        var listCity = new CitiesDTO();
        var wetherToTest = new Wether();
        wetherToTest.setWind(30);
        wetherToTest.setProbabilityOfPrecipitation(80);
        wetherToTest.setTemperature(20);
        listCity.setCity("CC");
        listCity.setWeather(wetherToTest);
        objectDTO.setDay("m");
        objectDTO.setCities(List.of(listCity));
        String city = "CC";
        String day = "m";

        var mockClient = mock(ClientSUN.class);
        when(mockClient.getAllinfo()).thenReturn(List.of(objectDTO));

        var toTest = service.checkWether(city,day,mockClient.getAllinfo());

        assertThat(Optional.empty());
    }

    @Test
    void check_Wether_OK(){
        var objectDTO= new OpenBikeDTO();
        var listCity = new CitiesDTO();
        var wetherToTest = new Wether();
        wetherToTest.setWind(30);
        wetherToTest.setProbabilityOfPrecipitation(10);
        wetherToTest.setTemperature(20);
        listCity.setCity("CC");
        listCity.setWeather(wetherToTest);
        objectDTO.setDay("m");
        objectDTO.setCities(List.of(listCity));
        String city = "CC";
        String day = "m";

        var mockClient = mock(ClientSUN.class);
        when(mockClient.getAllinfo()).thenReturn(List.of(objectDTO));

        var toTest = service.checkWether(city,day,mockClient.getAllinfo());

        assertEquals(false,toTest);

    }

    @Test
    void distance_Between_Cities_With_Wrong_Source_Name(){
        var listDistance = new DistanceDTO();
        listDistance.setDestination("M");
        listDistance.setSource("C");
        var mockClient = mock(ClientGEO.class);
        when(mockClient.getAllDistances()).thenReturn(List.of(listDistance));

        var toTest = service.roadBetweenCitys("F","C",mockClient.getAllDistances());

        assertThat(Optional.empty());
    }

    @Test
    void distance_Between_Cities_With_Wrong_Destination_Name(){
        var listDistance = new DistanceDTO();
        listDistance.setDestination("M");
        listDistance.setSource("C");
        var mockClient = mock(ClientGEO.class);
        when(mockClient.getAllDistances()).thenReturn(List.of(listDistance));

        var toTest = service.roadBetweenCitys("C","M",mockClient.getAllDistances());

        assertThat(Optional.empty());
    }

    @Test
    void distance_Between_Cities_OK(){
        var listDistance = new DistanceDTO();
        listDistance.setDestination("M");
        listDistance.setSource("C");
        listDistance.setDistance(100);
        var mockClient = mock(ClientGEO.class);
        when(mockClient.getAllDistances()).thenReturn(List.of(listDistance));

        var toTest = service.roadBetweenCitys("M","C",mockClient.getAllDistances());

        assertEquals(100,toTest.get().getDistance());
    }

}