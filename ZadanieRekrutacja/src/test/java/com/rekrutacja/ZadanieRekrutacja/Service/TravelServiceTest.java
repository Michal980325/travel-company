package com.rekrutacja.ZadanieRekrutacja.Service;

import com.rekrutacja.ZadanieRekrutacja.Model.*;
import com.rekrutacja.ZadanieRekrutacja.RestClient.ClientGEO;
import com.rekrutacja.ZadanieRekrutacja.RestClient.ClientSUN;
import com.rekrutacja.ZadanieRekrutacja.RestClient.ClientSpeed;
import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TravelServiceTest {


    @Test
    void factorial_OK_Numbers(){
        var mockGEO = mock(ClientGEO.class);
        var mockSPEED = mock(ClientSpeed.class);
        var mockSun = mock(ClientSUN.class);

        var toTest = new TravelService(mockGEO,mockSPEED,mockSun);
        assertEquals(120,toTest.factorial(5,1));
    }

    @Test
    void add_Hours_And_Minuts(){
        var mockGEO = mock(ClientGEO.class);
        var mockSPEED = mock(ClientSpeed.class);
        var mockSun = mock(ClientSUN.class);
        String h1 ="12:00";
        long minuts = 20;


        var toTest = new TravelService(mockGEO,mockSPEED,mockSun);
        assertEquals("12:20",toTest.addToHours(h1,minuts));
    }

    @Test
    void drive_From_City_To_City_With_Wrong_Name_Source_Name(){
        var mockGEO = mock(ClientGEO.class);
        var mockSPEED = mock(ClientSpeed.class);
        var mockSun = mock(ClientSUN.class);
        TravelByBus travelByBus = new TravelByBus();
        travelByBus.setDepartureTime("12:15");
        travelByBus.setDestinationTime("12:15");
        BusTravelModelDTO travelModel = new BusTravelModelDTO();
        travelModel.setSource("A");
        travelModel.setDestination("B");
        travelModel.setTravels(List.of(travelByBus));
        List<BusTravelModelDTO> list = List.of(travelModel);
        when(mockSPEED.getAllTravelsByBus()).thenReturn(list);
        String startCity="C";
        String finishCity="A";
        String time = "12:20";


        var toTest = new TravelService(mockGEO,mockSPEED,mockSun);
        toTest.driveTimeFromCityToCityByBus(startCity,finishCity,time);
        long timeDrive= toTest.driveTimeFromCityToCityByBus(startCity,finishCity,time);

        assertEquals(-1,timeDrive);
    }

    @Test
    void drive_From_City_To_City_With_Wrong_Name_Destination_Name(){
        var mockGEO = mock(ClientGEO.class);
        var mockSPEED = mock(ClientSpeed.class);
        var mockSun = mock(ClientSUN.class);
        TravelByBus travelByBus = new TravelByBus();
        travelByBus.setDepartureTime("12:15");
        travelByBus.setDestinationTime("12:15");
        BusTravelModelDTO travelModel = new BusTravelModelDTO();
        travelModel.setSource("A");
        travelModel.setDestination("B");
        travelModel.setTravels(List.of(travelByBus));
        List<BusTravelModelDTO> list = List.of(travelModel);
        when(mockSPEED.getAllTravelsByBus()).thenReturn(list);
        String startCity="A";
        String finishCity="C";
        String time = "12:20";


        var toTest = new TravelService(mockGEO,mockSPEED,mockSun);
        long timeDrive= toTest.driveTimeFromCityToCityByBus(startCity,finishCity,time);

        assertEquals(-1,timeDrive);
    }

    @Test
    void drive_From_City_To_City_OK(){
        var mockGEO = mock(ClientGEO.class);
        var mockSPEED = mock(ClientSpeed.class);
        var mockSun = mock(ClientSUN.class);
        TravelByBus travelByBus = new TravelByBus();
        travelByBus.setDepartureTime("12:15");
        travelByBus.setDestinationTime("12:25");
        BusTravelModelDTO travelModel = new BusTravelModelDTO();
        travelModel.setSource("A");
        travelModel.setDestination("B");
        travelModel.setTravels(List.of(travelByBus));
        List<BusTravelModelDTO> list = List.of(travelModel);
        when(mockSPEED.getAllTravelsByBus()).thenReturn(list);
        String startCity="A";
        String finishCity="B";
        String time = "12:00";


        var toTest = new TravelService(mockGEO,mockSPEED,mockSun);
        long timeDrive= toTest.driveTimeFromCityToCityByBus(startCity,finishCity,time);

        assertEquals(25,timeDrive);
    }

    @Test
    void get_List_Of_City(){
        var mockGEO = mock(ClientGEO.class);
        var mockSPEED = mock(ClientSpeed.class);
        var mockClient = mock(ClientSUN.class);
        var objectDTO= new OpenBikeDTO();
        var listCity = new CitiesDTO();
        var wetherToTest = new Wether();
        wetherToTest.setWind(100);
        wetherToTest.setProbabilityOfPrecipitation(0);
        wetherToTest.setTemperature(20);
        listCity.setCity("CC");
        listCity.setWeather(wetherToTest);
        objectDTO.setDay("m");
        objectDTO.setCities(List.of(listCity));

        when(mockClient.getAllinfo()).thenReturn(List.of(objectDTO));

        var toTest = new TravelService(mockGEO,mockSPEED,mockClient);
        List<String> listTest= toTest.findAllCity();
        List<String> checkList= List.of("CC");

        assertTrue(checkList.equals(listTest));
    }


    @Test
    void make_Map_With_Posible_Options_For_Bus(){
        var mockGEO = mock(ClientGEO.class);
        var mockSPEED = mock(ClientSpeed.class);
        var mockSun = mock(ClientSUN.class);
        var mockCity = List.of("C");
        TravelByBus travelByBus = new TravelByBus();
        travelByBus.setDepartureTime("12:15");
        travelByBus.setDestinationTime("12:25");
        BusTravelModelDTO travelModel = new BusTravelModelDTO();
        travelModel.setSource("A");
        travelModel.setDestination("C");
        travelModel.setTravels(List.of(travelByBus));
        TravelByBus travelByBus2 = new TravelByBus();
        travelByBus2.setDepartureTime("12:35");
        travelByBus2.setDestinationTime("12:45");
        BusTravelModelDTO travelModel2 = new BusTravelModelDTO();
        travelModel2.setSource("C");
        travelModel2.setDestination("B");
        travelModel2.setTravels(List.of(travelByBus2));
        List<BusTravelModelDTO> list = List.of(travelModel,travelModel2);

        when(mockSPEED.getAllTravelsByBus()).thenReturn(list);


        var toTest = new TravelService(mockGEO,mockSPEED,mockSun);
        Map<List<String> , Integer> toTestMap =  toTest.makeMapWithEvryPosibleRoadBetweenSourceAndDestination(mockCity,"A","B",
                true,"12:00",false,"m",null);

        List<String> checkList =List.of("A","C","B");



        Map<List<String> , Integer> checkMap = new LinkedHashMap<>();
        checkMap.put(checkList,45);

        assertTrue(checkMap.equals(toTestMap));

    }


    @Test
    void make_Map_With_Posible_Options_For_Bike(){
        var mockGEO = mock(ClientGEO.class);
        var mockSPEED = mock(ClientSpeed.class);
        var mockSun = mock(ClientSUN.class);
        var mockCity = List.of("C");
        var objectDTO= new OpenBikeDTO();
        var listCity = new CitiesDTO();
        var wetherToTest = new Wether();
        wetherToTest.setWind(30);
        wetherToTest.setProbabilityOfPrecipitation(10);
        wetherToTest.setTemperature(20);
        listCity.setCity("C");
        listCity.setWeather(wetherToTest);
        objectDTO.setDay("m");
        var listCity2 = new CitiesDTO();
        var wetherToTest2 = new Wether();
        wetherToTest2.setWind(30);
        wetherToTest2.setProbabilityOfPrecipitation(10);
        wetherToTest2.setTemperature(20);
        listCity2.setCity("A");
        listCity2.setWeather(wetherToTest);
        var listCity3 = new CitiesDTO();
        var wetherToTest3 = new Wether();
        wetherToTest3.setWind(30);
        wetherToTest3.setProbabilityOfPrecipitation(10);
        wetherToTest3.setTemperature(20);
        listCity3.setCity("B");
        listCity3.setWeather(wetherToTest);
        objectDTO.setCities(List.of(listCity,listCity2,listCity3));

        var listDistance1 = new DistanceDTO();
        listDistance1.setDestination("A");
        listDistance1.setSource("C");
        listDistance1.setDistance(100);

        var listDistance2 = new DistanceDTO();
        listDistance2.setDestination("C");
        listDistance2.setSource("B");
        listDistance2.setDistance(100);

        var listDistance3 = new DistanceDTO();
        listDistance3.setDestination("A");
        listDistance3.setSource("C");
        listDistance3.setDistance(400);


        when(mockSun.getAllinfo()).thenReturn(List.of(objectDTO));
        when(mockGEO.getAllDistances()).thenReturn(List.of(listDistance1,listDistance2,listDistance3));




        var toTest = new TravelService(mockGEO,mockSPEED,mockSun);
        Map<List<String> , Integer> toTestMap =  toTest.makeMapWithEvryPosibleRoadBetweenSourceAndDestination(mockCity,"A","B",
                false,"12:00",false,"m",null);

        List<String> checkList =List.of("A","C","B");



        Map<List<String> , Integer> checkMap = new LinkedHashMap<>();
        checkMap.put(checkList,600);

        assertTrue(checkMap.equals(toTestMap));

    }


}